<?php

namespace Bowling;

Class Game {

    private int $rounds;
    private int $score = 0;

    private bool $spare = false;

    function __construct(int $rounds){
        $this->rounds = $rounds;
    }

    public function addFrame(string $round): void
    {
        $spare = false;
        if ($this->spare) {
            $spare = true;
            $this->spare = false;
        }

        if ($round[1] === '/') {
            $this->spare = true;

            $roundTwoScore = 10 - $round[0];

            $this->score += ($spare ? $round[0] * 2 : $round[0]);
            $this->score += $roundTwoScore;
        } else {
            $this->score += ($spare ? $round[0] * 2 : $round[0]);;
            $this->score += $round[1];
        }

        if (strlen ($round) === 3) {
            $this->score += ($this->spare ? $round[2] * 2 : $round[2]);
        }
    }

    public function getScore(): int {
        return $this->score;
    }
}

